// https://matrix.org/docs/spec/client_server/latest#sharing-keys-between-devices

use base64::decode;
use crypto::{
    aes::{ctr, KeySize::KeySize256},
    hmac::Hmac,
    pbkdf2::pbkdf2,
    sha2::Sha512,
};
use serde::{Deserialize, Serialize};
use std::{collections::BTreeMap, convert::TryInto, fs::File, io::prelude::*, str};

fn rev_split_off<T>(tail: &mut Vec<T>, at: usize) -> Vec<T> {
    let mut head = tail.split_off(at);
    std::mem::swap(&mut head, tail);
    head
}

#[derive(Debug)]
struct KeyFile {
    version: u8,
    salt: [u8; 16],
    init_vec: [u8; 16],
    nb_of_rounds: u32,
    signature: [u8; 32],
    enc_payload: Vec<u8>,
}

impl KeyFile {
    fn new(mut payload: Vec<u8>) -> anyhow::Result<Self> {
        Ok(KeyFile {
            version: payload.remove(0),
            salt: rev_split_off(&mut payload, 16)[..].try_into()?,
            init_vec: rev_split_off(&mut payload, 16)[..].try_into()?,
            nb_of_rounds: u32::from_be_bytes(rev_split_off(&mut payload, 4)[..].try_into()?),
            signature: payload.split_off(payload.len() - 32)[..].try_into()?,
            enc_payload: payload,
        })
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SessionData {
    algorithm: String,
    forwarding_curve25519_key_chain: Vec<String>,
    room_id: String,
    pub sender_key: String,
    sender_claimed_keys: BTreeMap<String, String>,
    pub session_id: String,
    pub session_key: String,
}

pub fn decrypt(path: String, password: String) -> anyhow::Result<Vec<SessionData>> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<_> = contents.lines().collect();
    assert_eq!(
        *lines.first().unwrap(),
        "-----BEGIN MEGOLM SESSION DATA-----"
    );
    assert_eq!(*lines.last().unwrap(), "-----END MEGOLM SESSION DATA-----");
    let split_b64_payload = &lines[1..lines.len() - 1];
    let b64_payload = split_b64_payload.concat();
    let key_file = KeyFile::new(decode(b64_payload)?)?;
    assert_eq!(0x01, key_file.version);
    let mut mac = Hmac::new(Sha512::new(), password.as_bytes());
    let mut key = [0; 64];
    pbkdf2(&mut mac, &key_file.salt, key_file.nb_of_rounds, &mut key);
    let mut cipher = ctr(KeySize256, &key[..32], &key_file.init_vec);
    let len = key_file.enc_payload.len();
    let mut decrypted_bytes = Vec::with_capacity(len);
    decrypted_bytes.resize_with(len, Default::default);
    cipher.process(&key_file.enc_payload, &mut decrypted_bytes);
    let decrypted = str::from_utf8(&decrypted_bytes)?;
    Ok(serde_json::from_str(decrypted)?)
}
