mod lib;
use lib::decrypt;
use std::{env, process::exit};

fn main() -> anyhow::Result<()> {
    let (path, password) = match (env::args().nth(1), env::args().nth(2)) {
        (Some(a), Some(b)) => (a, b),
        _ => {
            eprintln!(
                "Usage: {} <file_path> <password>",
                env::args().nth(0).unwrap()
            );
            exit(1)
        }
    };
    let decrypted = decrypt(path, password)?;
    println!("{:#?}", decrypted);
    Ok(())
}
