use std::{collections::HashMap, convert::TryFrom, env, process::exit};

use matrix_key_export_file::decrypt as decrypt_key_file;
use matrix_sdk::{
    self,
    api::r0::message::get_message_events::{Direction, Request},
    identifiers::RoomId,
    Client,
};
use olm_rs::inbound_group_session::OlmInboundGroupSession;
use ruma_events::{
    collections::all::RoomEvent::RoomEncrypted,
    room::encrypted::{
        EncryptedEvent, EncryptedEventContent, MegolmV1AesSha2Content,
        OlmV1Curve25519AesSha2Content,
    },
};
use url::Url;

fn decrypt(
    content: EncryptedEventContent,
    sessions: &HashMap<String, OlmInboundGroupSession>,
) -> String {
    use EncryptedEventContent::*;
    match content {
        OlmV1Curve25519AesSha2(OlmV1Curve25519AesSha2Content { .. }) => todo!(),
        MegolmV1AesSha2(MegolmV1AesSha2Content {
            ciphertext,
            sender_key,
            ..
        }) => {
            println!("{:#?}", (&ciphertext, &sender_key));
            sessions
                .get(&sender_key)
                .expect("no session found for this sender_key")
                .decrypt(ciphertext)
                .expect("couldn't decrypt ciphertext")
                .0
        }
        _ => unimplemented!(),
    }
}

async fn dump_room(
    homeserver_url: Url,
    username: String,
    password: String,
    room_id: RoomId,
    key_file_path: String,
    key_file_pass: String,
) -> anyhow::Result<()> {
    let client = Client::new(homeserver_url)?;

    client.login(username, password, None, None).await?;
    let mut sync = client.sync(Default::default()).await?;
    let prev_batch = sync
        .rooms
        .join
        .remove(&room_id)
        .expect("No room with provided RoomId")
        .timeline
        .prev_batch
        .expect("No sync token for room");
    let messages = client
        .room_messages(Request {
            room_id,
            from: prev_batch,
            to: None,
            dir: Direction::Backward,
            limit: None,
            filter: None,
        })
        .await?;

    let mut sessions = decrypt_key_file(key_file_path, key_file_pass)?;
    println!("{:#?}", sessions);
    let mut session_map = HashMap::with_capacity(sessions.len());
    session_map.extend(sessions.drain(..).map(|session| {
        (
            session.sender_key,
            OlmInboundGroupSession::import(&session.session_key).unwrap(),
        )
    }));

    for event in messages.chunk {
        let typed_event = match event.deserialize()? {
            RoomEncrypted(EncryptedEvent { content, .. }) => decrypt(content, &session_map),
            x => format!("{:#?}", x),
        };
        println!("{}", typed_event);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let (homeserver_url, username, password, room, key_file_path, key_file_pass) = match (
        env::args().nth(1),
        env::args().nth(2),
        env::args().nth(3),
        env::args().nth(4),
        env::args().nth(5),
        env::args().nth(6),
    ) {
        (Some(a), Some(b), Some(c), Some(d), Some(e), Some(f)) => (a, b, c, d, e, f),
        _ => {
            eprintln!(
                "Usage: {} <homeserver_url> <username> <password> <room> <key_file> <key_file_passphrase>",
                env::args().nth(0).unwrap()
            );
            exit(1)
        }
    };

    dump_room(
        homeserver_url.parse()?,
        username,
        password,
        RoomId::try_from(&room[..])?,
        key_file_path,
        key_file_pass,
    )
    .await
}
